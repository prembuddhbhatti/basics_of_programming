process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	let rgx = /([#])\w+/g;
	const mp = new Map();
	for (i = 1; i <= inputArr[0]; i++) {
		let match = inputArr[i].match(rgx);
		match.forEach((ele) => {
			let m = mp.get(ele);
			if (m) {
				mp.set(ele, m + 1);
			} else {
				mp.set(ele, 1);
			}
		});
	}
	const srt_a_mp = new Map([...mp.entries()].sort());
	const srt_mp = new Map([...srt_a_mp.entries()].sort((a, b) => b[1] - a[1]));
	let ct = 0;
	for (let [key, value] of srt_mp) {
		ct++;
		console.log(`${key}`);
		if (ct == 5) {
			break;
		}
	}
}
