performed in NodeJs

steps:
    SET stdin_input = ""

    process.stdin.on("data", function (input) {
        READ input
        SET stdin_input += input
    });

    process.stdin.on("end", function () {
        CALL main with stdin_input
    });

    function main(input) {
        SET inputArr = input.split("\n")
        SET  rgx = /([#])\w+/g
        SET mp = new Map()

        FOR (i = 1; i <= inputArr[0]; i++) 
            SET match = inputArr[i].match(rgx)
            
            match.forEach((ele) => {
                
                SET m = mp.get(ele);
                
                IF (m) THEN
                    mp.set(ele, m + 1)
                ELSE
                    mp.set(ele, 1)
                END IF

            });

        END FOR 

        SET srt_a_mp = new Map([...mp.entries()].sort())

        SET srt_mp = new Map([...srt_a_mp.entries()].sort((a, b) => b[1] - a[1]))

        SET ct = 0
        FOR EACH [key, value] of srt_mp
            INCREMENT ct
            PRINT key

            IF ct == 5 THEN
                BREAK
            END IF
        END FOR
    }