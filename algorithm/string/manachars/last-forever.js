process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function convertString(s) {
	let newString = "@";

	for (i = 0; i < s.length; i++) {
		newString += "#" + s.substr(i, 1);
	}

	newString += "#$";
	return newString;
}

function longestPalindrome(s, x) {
	let Q = convertString(s);
	P = Array(Q.length).fill(0);
	let c = 0,
		r = 0;
	f = 1;
	for (i = 1; i < Q.length - 1; i++) {
		// find the corresponding letter in the palidrome subString
		let iMirror = c - (i - c);
		if (r > i) {
			//P[i] = //min(r - i, P[iMirror]);
			if (r - 1 < P[iMirror]) {
				P[i] = r - 1;
			} else {
				P[i] = P[iMirror];
			}
		}

		// expanding around center i
		while (Q[i + 1 + P[i]] == Q[i - 1 - P[i]]) {
			P[i] += 1;
		}
		// Update c,r in case if the palindrome centered at i expands past r,
		if (i + P[i] > r) {
			c = i; // next center = i
			r = i + P[i];
		}
	}

	// Find the longest palindrome length in p.

	let maxPalindrome = 0;
	let centerIndex = 0;
	let coutn = 0;
	//console.log(P);
	for (i = 1; i < Q.length - 1; i++) {
		if (P[i] == x) {
			centerIndex = i;
			coutn++;
		}
	}
	//console.log(P)
	console.log(coutn);
	return 1;
}

function main(input) {
	let inputArr = input.split("\n");
	let s = inputArr[0];
	let q = parseInt(inputArr[1]);

	for (let i = 2; i < q + 2; i++) {
		let arr = inputArr[i].split(" ");
		let l = parseInt(arr[0], 10);
		let x = parseInt(arr[1], 10);
		let ss = s.slice(l);
		//console.log(ss)
		longestPalindrome(ss, x);
	}
}
