importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN
try {
	function string_sort(str, i, k) {
		var i, j;
		while (i <= k) {
			j = i + 1;
			while (j <= k) {
				if (str[j] > str[i]) {
					//console.log(j + str[j] + "<" + i + str[i]);
					var temp = str[i];
					str[i] = str[j];
					str[j] = temp;
				}
				j++;
			}
			i++;
		}
		return str.join("");
	}

	let t = sc.next();
	while (t--) {
		let str = sc.next();
		let i = sc.nextInt();
		let k = sc.nextInt();
		let st = string_sort(str.split(""), i, k);
		System.out.println(st);
	}
} catch (e) {
	System.out.println(e + "");
}
