performed in Js

steps:-
    function string_sort(str, i, k) {
		SET i, j
		
        WHILE (i <= k) 
			
            SET j = i + 1
			
            WHILE (j <= k) {
				
                IF (str[j] > str[i]) THEN
					
					SET var temp = str[i]
					SET str[i] = str[j]
					SET str[j] = temp
				
                END IF

				INCREMENT j

			END WHILE

			INCREMENT i

		END WHILE

		RETURN str.join("");
	}

    {
        in the main function

        READ t
        
        WHILE (t--) 
            
            READ str
            READ i
            READ k
            SET st = CALL string_sort with (str.split(""), i, k) RETURNING string
            
            PRINT st

        END WHILE

    }