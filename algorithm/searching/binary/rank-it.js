process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	//console.log(inputArr)
	let arr = inputArr[1].split(" ");
	let srt_arr = arr.sort((a, b) => {
		return parseInt(a) - parseInt(b);
	});
	//console.log(srt_arr)
	let q = parseInt(inputArr[2]);
	//let q_arr = inputArr.splice(3,inputArr.length);
	for (i = 3; i < q + 3; i++) {
		let ind = srt_arr.indexOf(inputArr[i]) + 1;
		process.stdout.write(ind + "\n");
	}
}
