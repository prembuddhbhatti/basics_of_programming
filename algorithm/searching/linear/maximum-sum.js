process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	let arr = inputArr[1].split(" ");
	let ct = 0;
	let sum = 0;
	for (i = 0; i < inputArr[0]; i++) {
		let n = parseInt(arr[i]);
		if (n >= 1) {
			ct++;
			sum += n;
		}
	}
	process.stdout.write(sum + " " + ct);
}
