performed in JS

steps:-
    {
        in main function

        READ t
        WHILE (t--) 
            READ n 
            READ l 
            SET n = parseInt(n);
            SET md = n % 3;
            IF (md != 0) THEN
                SET min = 2 * Math.pow(10, l) + n;
                
                FOR (i = 0; i <= l; i++) 
                    SET x = n.toString();

                    SET ans = x.substring(0, i) + "1" + x.substring(i, x.length);

                    SET nn = parseInt(ans);
                    
                    IF (nn % 3 == 0 && nn < min) THEN
                        SET min = nn;
                    END IF

                END FOR

                FOR (i = 0; i <= l; i++) 
                    SET x = n.toString();

                    SET ans = x.substring(0, i) + "2" + x.substring(i, x.length);

                    SET nn = parseInt(ans);
                    
                    IF (nn % 3 == 0 && nn < min) THEN
                        SET min = nn;
                    END IF
                    
                END FOR
                System.out.println(min + "");
            ELSE 
                System.out.println(n + "");
            END IF
        END WHILE

    }