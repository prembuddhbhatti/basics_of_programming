importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN
try {
	let t = sc.next();
	while (t--) {
		let n = sc.next();
		let l = n.length();
		n = parseInt(n);
		let md = n % 3;
		if (md != 0) {
			let min = 2 * Math.pow(10, l) + n;
			for (i = 0; i <= l; i++) {
				let x = n.toString();

				let ans = x.substring(0, i) + "1" + x.substring(i, x.length);

				let nn = parseInt(ans);
				if (nn % 3 == 0 && nn < min) {
					min = nn;
				}
			}
			for (i = 0; i <= l; i++) {
				let x = n.toString();

				let ans = x.substring(0, i) + "2" + x.substring(i, x.length);

				let nn = parseInt(ans);
				if (nn % 3 == 0 && nn < min) {
					min = nn;
				}
			}
			System.out.println(min + "");
		} else {
			System.out.println(n + "");
		}
	}
} catch (e) {
	System.out.println(e + "");
}
