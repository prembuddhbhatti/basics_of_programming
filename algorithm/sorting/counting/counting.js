function counting_sort(A, Aux, sortedA, N) {
	// First, find the maximum value in A[]
	let K = 0;
	for (let i = 0; i < N; i++) {
		K > A[i] ? (K = K) : (K = A[i]);
	}

	// Initialize the elements of Aux[] with 0
	for (let i = 0; i <= K; i++) {
		Aux[i] = 0;
	}

	// Store the frequencies of each distinct element of A[],
	// by mapping its value as the index of Aux[] array
	for (let i = 0; i < N; i++) {
		Aux[A[i]]++;
	}

	let j = 0;
	for (let i = 0; i <= K; i++) {
		let tmp = Aux[i];
		// Aux stores which element occurs how many times,
		// Add i in sortedA[] according to the number of times i occured in A[]
		while (tmp--) {
			//cout << Aux[i] << endl;
			sortedA[j] = i;
			j++;
		}
	}
}
let ar = [3, 5, 2, 6, 45, 1, 3];
let Aux = [];
let sortedA = [];
console.log(ar);
counting_sort(ar, Aux, sortedA, ar.length);
let srt = sortedA.filter((c, index) => {
	return sortedA.indexOf(c) === index;
});
console.log(Aux);
console.log(sortedA);
console.log(srt);
