/*
// Below is a sample code to process input from STDIN.
// Equivalent in effect to the Java declaration import java.io.*;
importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System['in']);					// Reading input from STDIN

var my_name = sc.nextLine();
System.out.println("Hi, " + my_name + ".");			// Writing output to STDOUT
// End of input processing code.

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here
importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN
try {
	function counting_sort(A, Aux, sortedA, N) {
		// First, find the maximum value in A[]
		let K = 0;
		for (let i = 0; i < N; i++) {
			K > A[i] ? (K = K) : (K = A[i]);
		}

		// Initialize the elements of Aux[] with 0
		for (let i = 0; i <= K; i++) {
			Aux[i] = 0;
		}

		// Store the frequencies of each distinct element of A[],
		// by mapping its value as the index of Aux[] array
		for (let i = 0; i < N; i++) {
			Aux[A[i]]++;
		}

		let j = 0;
		for (let i = 0; i <= K; i++) {
			let tmp = Aux[i];
			// Aux stores which element occurs how many times,
			// Add i in sortedA[] according to the number of times i occured in A[]
			while (tmp--) {
				//cout << Aux[i] << endl;
				sortedA[j] = i;
				j++;
			}
		}
	}

	let n = sc.nextInt();
	let arr = [];

	while (n--) {
		let a = sc.nextInt();
		arr.push(a);
	}

	let Aux = [];
	let sortedA = [];
	counting_sort(arr, Aux, sortedA, arr.length);

	let srt = sortedA.filter((c, index) => {
		return sortedA.indexOf(c) === index;
	});
	// System.out.println(srt[0]+" ");
	for (i = 0; i < srt.length; i++) {
		System.out.println(srt[i] + " " + Aux[srt[i]]);
	}
} catch (e) {
	System.out.println(e + "");
}
