importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

function bubble_sort(arr, n) {
	let temp;
	let swp = 0;
	for (let k = 0; k < n - 1; k++) {
		// (n-k-1) is for ignoring comparisons of elements which have already been compared in earlier iterations

		for (let i = 0; i < n - k - 1; i++) {
			if (arr[i] > arr[i + 1]) {
				// here swapping of positions is being done.
				swp++;
				temp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = temp;
			}
		}
	}
	//console.log(swp);
	//return arr;
}

var sc = new Scanner(System["in"]); // Reading input from STDIN

let t = sc.next();
try {
	while (t--) {
		let n = sc.nextInt();
		let m = sc.nextInt();
		let arr = [];
		for (i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}
		/* arr.sort((a, b) => {
			return a-b;
		}); */
		bubble_sort(arr, arr.length);

		let min = 0;
		let max = 0;

		for (i = 0; i < n - m; i++) {
			min += arr[i];
		}
		//System.out.println(min+"");
		for (i = n - 1; i >= m; i--) {
			max += arr[i];
		}
		//System.out.println(max+"");

		let ans = max - min;
		System.out.println(ans + "");
	}
} catch (e) {
	System.out.println(e + "");
}
