function bubble_sort(arr, n) {
	let temp;
	let swp = 0;
	for (let k = 0; k < n - 1; k++) {
		// (n-k-1) is for ignoring comparisons of elements which have already been compared in earlier iterations

		for (let i = 0; i < n - k - 1; i++) {
			if (arr[i] > arr[i + 1]) {
				// here swapping of positions is being done.
				swp++;
				temp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = temp;
			}
		}
	}
	console.log(swp);
	//return arr;
}

let ar = [3, 5, 2, 6, 45, 1];
console.log(ar);
bubble_sort(ar, ar.length);
console.log(ar);
