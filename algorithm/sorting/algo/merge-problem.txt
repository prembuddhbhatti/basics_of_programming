performed in Js 

steps:-
    SET count = 1
    function merge(A, start, mid, end) {
        SET p = start
        SET q = mid + 1

        SET Arr = []
        SET k = 0

        FOR (i = start; i <= end; i++) 
            IF (p > mid) THEN
                SET Arr[k++] = A[q++]
            ELSE IF (q > end) THEN
                SET Arr[k++] = A[p++]
            ELSE IF (A[p] < A[q]) THEN
                SET Arr[k++] = A[p++]
            ELSE 
                SET count += mid - p + 1
                SET Arr[k++] = A[q++]
            END IF
        END FOR

        FOR (p = 0; p < k; p++)
            SET A[start++] = Arr[p]
        END FOR
    }

    function merge_sort(A, start, end) {
        IF (start < end) THEN
            SET mid = (start + end) / 2; 
            
            CALL merge_sort with (A, start, mid)
            
            CALL merge_sort with (A, mid + 1, end)

            CALL merge with (A, start, mid, end)
        }
    }

    {
        in main function

        READ n;
        SET arr = [];
        FOR (i = 0; i < n; i++) 
            READ arr[i]
        END FOR
        CALL merge_sort with (arr, 0, arr.length - 1)
        PRINT count + ""
    }