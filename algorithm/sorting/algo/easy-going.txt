performed in js 

steps:- 
    
    function bubble_sort(arr, n) {
        SET temp
        SET swp = 0
        FOR (let k = 0; k < n - 1; k++) 
            
            FOR (let i = 0; i < n - k - 1; i++) 
                IF(arr[i] > arr[i + 1]) THEN
                    
                    INCREMENT swp
                    SET temp = arr[i]
                    SET arr[i] = arr[i + 1];
                    SET arr[i + 1] = temp;
                END IF
            END FOR
        END FOR
    }

 

    {
        in the mani function 

        READ t;

        WHILE (t--) 
            READ n;
            READ m;
            SET arr = []
            
            FOR (i = 0; i < n; i++) 
                READ arr[i]
            END FOR
            
            CALL bubble_sort with (arr, arr.length)

            SET min = 0
            SET max = 0

            FOR (i = 0; i < n - m; i++) 
                SET min += arr[i]
            END FOR
            
            FOR (i = n - 1; i >= m; i--) 
                SET max += arr[i]
            END FOR
            

            SET ans = max - min
            PRINT ans + ""
        END WHILE
    }