performed in JS 

steps:- 
    
    function counting_sort(A, Aux, sortedA, N) {
        
        SET K = 0
        
        FOR (let i = 0; i < N; i++) 
            K > A[i] ? (K = K) : (K = A[i]);
        END FOR

        
        FOR (let i = 0; i <= K; i++) 
            SET Aux[i] = 0
        END FOR

        
        FOR (let i = 0; i < N; i++) 
            INCREMENT Aux[A[i]]
        END FOR

        SET j = 0

        FOR (let i = 0; i <= K; i++) 
            SET tmp = Aux[i]
           
            WHILE (tmp--) 
                
                SET sortedA[j] = i
                INCREMENT j

            END WHILE

        END FOR
    }


    {
        in main function

        READ n 
        SET arr = []

        WHILE (n--) 
            
            READ a 
            arr.push(a);

        END WHILE

        SET Aux = []
        SET sortedA = []
        CALL counting_sort with (arr, Aux, sortedA, arr.length)

        SET srt = sortedA.filter((c, index) => {
            return sortedA.indexOf(c) === index;
        });
        
        FOR (i = 0; i < srt.length; i++) 
            
            PRINT srt[i] + " " + Aux[srt[i]]

        END FOR
    }