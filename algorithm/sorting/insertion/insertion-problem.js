/*
// Below is a sample code to process input from STDIN.
// Equivalent in effect to the Java declaration import java.io.*;
importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System['in']);					// Reading input from STDIN

var my_name = sc.nextLine();
System.out.println("Hi, " + my_name + ".");			// Writing output to STDOUT
// End of input processing code.

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here
importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN

function insertion_sort(A, n) {
	let swp = 0;
	for (let i = 0; i < n; i++) {
		/*storing current element whose left side is checked for its 
             correct position .*/

		let temp = A[i];
		let j = i;

		/* check whether the adjacent element in left side is greater or
            less than the current element. */

		while (j > 0 && temp < A[j - 1]) {
			// moving the left side element to one position forward.
			A[j] = A[j - 1];
			j = j - 1;
			swp++;
		}
		// moving current element to its  correct position.
		if (j !== i) {
			A[j] = temp;
			swp++;
		}
	}
	//console.log(swp);
}

try {
	let n = sc.nextInt();
	let arr = [];
	for (i = 0; i < n; i++) {
		arr[i] = sc.nextInt();
	}
	let srt = [].concat(arr);
	insertion_sort(srt, n);

	let ans = "";
	//System.out.println(arr.toString());
	//System.out.println(srt.toString());
	for (i = 0; i < n; i++) {
		ans += srt.indexOf(arr[i]) + 1 + " ";
	}

	System.out.println(ans);
} catch (e) {
	System.out.println(e + "");
}
