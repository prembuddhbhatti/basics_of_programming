function insertion_sort(A, n) {
	let swp = 0;
	for (let i = 0; i < n; i++) {
		/*storing current element whose left side is checked for its 
             correct position .*/

		let temp = A[i];
		let j = i;

		/* check whether the adjacent element in left side is greater or
            less than the current element. */

		while (j > 0 && temp < A[j - 1]) {
			// moving the left side element to one position forward.
			A[j] = A[j - 1];
			j = j - 1;
			swp++;
		}
		// moving current element to its  correct position.
		if (j !== i) {
			A[j] = temp;
			swp++;
		}
	}
	console.log(swp);
}

let ar = [3, 5, 2, 6, 45, 1];
console.log(ar);
insertion_sort(ar, ar.length);
console.log(ar);
