function partition(A, start, end) {
	let i = start + 1;
	let piv = A[start]; //make the first element as pivot element.
	for (let j = start + 1; j <= end; j++) {
		/*rearrange the array by putting elements which are less than pivot
       on one side and which are greater that on other. */

		if (A[j] < piv) {
			temp = A[i];
			A[i] = A[j];
			A[j] = temp;
			i += 1;
		}
	}
	//put the pivot element in its proper place.
	temp = A[start];
	A[start] = A[i - 1];
	A[i - 1] = temp;
	return i - 1; //return the position of the pivot
}

function quick_sort(A, start, end) {
	if (start < end) {
		//stores the position of pivot element
		let piv_pos = partition(A, start, end);
		quick_sort(A, start, piv_pos - 1); //sorts the left side of pivot.
		quick_sort(A, piv_pos + 1, end); //sorts the right side of pivot.
	}
}

let ar = [3, 5, 2, 6, 45, 1];
console.log(ar);
quick_sort(ar, 0, ar.length - 1);
console.log(ar);
