importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN
try {
	function partition(A, start, end) {
		let i = start + 1;
		let piv = A[start]; //make the first element as pivot element.
		for (let j = start + 1; j <= end; j++) {
			/*rearrange the array by putting elements which are less than pivot
        on one side and which are greater that on other. */

			if (A[j] < piv) {
				temp = A[i];
				A[i] = A[j];
				A[j] = temp;
				i += 1;
			}
		}
		//put the pivot element in its proper place.
		temp = A[start];
		A[start] = A[i - 1];
		A[i - 1] = temp;
		return i - 1; //return the position of the pivot
	}

	function quick_sort(A, start, end) {
		if (start < end) {
			//stores the position of pivot element
			let piv_pos = partition(A, start, end);
			quick_sort(A, start, piv_pos - 1); //sorts the left side of pivot.
			quick_sort(A, piv_pos + 1, end); //sorts the right side of pivot.
		}
	}

	let n = sc.nextInt();
	let arr = [];

	while (n--) {
		let a = sc.nextInt();
		arr.push(a);
	}
	quick_sort(arr, 0, arr.length);

	let ans = "";

	for (i = 0; i < arr.length; i++) {
		ans += arr[i] + " ";
		//System.out.println(arr[i]+"");
	}

	System.out.println(ans);
} catch (e) {
	System.out.println(e + "");
}
