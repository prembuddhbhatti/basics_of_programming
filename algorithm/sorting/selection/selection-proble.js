importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN

function selection_sort(arr, n, x) {
	let min;
	let swp = 0;
	for (let i = 0; i < x; i++) {
		// assuming the first element to be the minimum of the unsorted array .
		min = i;

		// gives the effective size of the unsorted  array .

		for (j = i + 1; j < n; j++) {
			if (arr[j] < arr[min]) {
				//finds the minimum element
				min = j;
			}
		}
		// putting minimum element on its proper position.
		if (min !== i) {
			temp = arr[min];
			arr[min] = arr[i];
			arr[i] = temp;
			swp++;
		}
	}
	//console.log(swp);
}

try {
	let n = sc.nextInt();
	let x = sc.nextInt();
	let arr = [];

	for (i = 0; i < n; i++) {
		arr[i] = sc.nextInt();
	}

	selection_sort(arr, n, x);
	let ans = "";

	for (i = 0; i < n; i++) {
		ans += arr[i] + " ";
	}

	System.out.println(ans);
} catch (e) {
	System.out.println(e + "");
}
