const heapsort = (arr) => {
	const a = [...arr];
	let l = a.length;

	const heapify = (a, i) => {
		const left = 2 * i + 1;
		const right = 2 * i + 2;
		let max = i;
		if (left < l && a[left] > a[max]) {
			max = left;
		}
		if (right < l && a[right] > a[max]) {
			max = right;
		}
		if (max !== i) {
			[a[max], a[i]] = [a[i], a[max]];
			heapify(a, max);
		}
	};

	for (let i = Math.floor(l / 2); i >= 0; i -= 1) {
		heapify(a, i);
	}
	for (i = a.length - 1; i > 0; i--) {
		[a[0], a[i]] = [a[i], a[0]];
		l--;
		heapify(a, 0);
	}
	if (a.length < 3) {
		console.log("-1");
	} else {
		ans = "";
		for (i = a.length - 1; i >= a.length - 3; i--) {
			ans += a[i] + " ";
		}
		console.log(ans);
	}
};

process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	let ar = [];
	for (let i = 1; i < inputArr.length; i++) {
		ar.push(parseInt(inputArr[i]));
		//console.log(ar)
		heapsort(ar);
	}
}
