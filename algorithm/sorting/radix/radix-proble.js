/*
// Sample code to perform I/O:

process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
    stdin_input += input;                               // Reading input from STDIN
});

process.stdin.on("end", function () {
   main(stdin_input);
});

function main(input) {
    process.stdout.write("Hi, " + input + ".\n");       // Writing output to STDOUT
}

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here
process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	let arr = inputArr[1].split(" ");
	radixSort(arr);
}

const getNum = (num, index) => {
	const strNum = String(num);
	let end = strNum.length - 1;
	const foundNum = strNum[end - index];

	if (foundNum === undefined) return 0;
	else return foundNum;
};

const largestNum = (arr) => {
	let largest = "0";

	arr.forEach((num) => {
		const strNum = String(num);

		if (strNum.length > largest.length) largest = strNum;
	});

	return largest.length;
};

const radixSort = (arr) => {
	let maxLength = largestNum(arr);

	for (let i = 0; i < maxLength; i++) {
		let buckets = Array.from({ length: 10 }, () => []);

		for (let j = 0; j < arr.length; j++) {
			let num = getNum(arr[j], i);

			if (num !== undefined) {
				buckets[num].push(arr[j]);
			}
		}
		arr = buckets.flat();
		console.log(arr.toString().replaceAll(",", " "));
	}

	return arr;
};
