importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]);
let count = 1;

function merge(A, start, mid, end) {
	//stores the starting position of both parts in temporary variables.
	let p = start,
		q = mid + 1;

	let Arr = [],
		k = 0;

	for (i = start; i <= end; i++) {
		if (p > mid) {
			//checks if first part comes to an end or not .
			Arr[k++] = A[q++];
		} else if (q > end) {
			//checks if second part comes to an end or not
			Arr[k++] = A[p++];
		} else if (A[p] < A[q]) {
			//checks which part has smaller element.
			Arr[k++] = A[p++];
		} else {
			count += mid - p + 1;
			Arr[k++] = A[q++];
		}
	}
	for (p = 0; p < k; p++) {
		/* Now the real array has elements in sorted manner including both 
           parts.*/
		A[start++] = Arr[p];
	}
}

function merge_sort(A, start, end) {
	if (start < end) {
		mid = (start + end) / 2; // defines the current array in 2 parts .
		merge_sort(A, start, mid); // sort the 1st part of array .
		merge_sort(A, mid + 1, end); // sort the 2nd part of array.

		// merge the both parts by comparing elements of both the parts.
		merge(A, start, mid, end);
	}
}

try {
	let n = sc.nextInt();
	let arr = [];
	for (i = 0; i < n; i++) {
		arr[i] = sc.nextInt();
	}
	merge_sort(arr, 0, arr.length - 1);
	System.out.println(count + "");
} catch (e) {
	System.out.println(e + "");
}
