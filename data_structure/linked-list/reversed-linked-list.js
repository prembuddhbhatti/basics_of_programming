class Node {
	// constructor
	constructor(element) {
		this.element = element;
		this.next = null;
	}
}
// linkedlist class
class LinkedList {
	constructor() {
		this.head = null;
		this.size = 0;
	}

	// adds an element at the end
	// of list
	add(element) {
		// creates a new node
		var node = new Node(element);

		// to store current node
		var current;

		// if list is Empty add the
		// element and make it head
		if (this.head == null) {
			this.head = node;
		} else {
			current = this.head;

			// iterate to the end of the
			// list
			while (current.next) {
				current = current.next;
			}

			// add node
			current.next = node;
		}
		this.size++;
	}

	// prints the list items
	printList() {
		var curr = this.head;
		var str = "";
		while (curr) {
			if (Array.isArray(curr.element)) {
				let r_arr = curr.element.reverse();
				str += r_arr.toString().replaceAll(",", " ") + " ";
			} else {
				str += curr.element + " ";
			}

			curr = curr.next;
		}
		console.log(str);
	}
}

process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input;
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	let inputArr = input.split("\n");
	let arr = inputArr[1].split(" ");
	let s_arr = [];
	let flag = 0;
	let ll = new LinkedList();
	for (i = 0; i < arr.length; i++) {
		if (parseInt(arr[i] & 1)) {
			if (flag) {
				ll.add(s_arr);
				s_arr = [];
				flag = 0;
			}
			ll.add(arr[i]);
		} else {
			flag = 1;
			s_arr.push(arr[i]);
		}
	}
	if (flag == 1) {
		ll.add(s_arr);
		s_arr = [];
		flag = 0;
	}

	ll.printList();
}
