#include <bits/stdc++.h>
using namespace std;

vector<int> funGame(vector<int> arr)
{
    // Write your code here
    int a_index = 0;
    int b_index = arr.size() - 1;
    vector<int> out;

    while (a_index < arr.size() && b_index > -1)
    {

        // cout << "a_index: "<< a_index << " b_index: "<<b_index<< endl;

        if (arr[a_index] > arr[b_index])
        {
            out.push_back(1);
            b_index--;
        }
        else if (arr[a_index] < arr[b_index])
        {
            out.push_back(2);
            a_index++;
        }
        else
        {
            out.push_back(0);
            a_index++;
            b_index--;
        }
    }

    return out;
}

int main()
{

    ios::sync_with_stdio(0);
    cin.tie(0);
    int n;
    cin >> n;
    vector<int> arr(n);
    for (int i_arr = 0; i_arr < n; i_arr++)
    {
        cin >> arr[i_arr];
    }

    vector<int> out_;
    out_ = funGame(arr);
    cout << out_[0];
    for (int i_out_ = 1; i_out_ < out_.size(); i_out_++)
    {
        cout << " " << out_[i_out_];
    }
}