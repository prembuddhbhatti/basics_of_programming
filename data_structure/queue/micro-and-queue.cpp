#include <iostream>
#include <cstdio>

using namespace std;
int count = 0;

void enqueue(int queue[], int element, int &rear, int arraySize)
{
    if (rear == arraySize) // Queue is full
        printf("OverFlow\n");
    else
    {
        queue[rear] = element; // Add the element to the back
        rear++;
        count++;
    }
}

int dequeue(int queue[], int &front, int rear)
{
    if (front == rear)
    {
        count = 0;
        return -1;
    }
    else
    {
        int d = queue[front];
        queue[front] = 0; // Delete the front element
        front++;
        count--;
        return d;
    }
}

int Front(int queue[], int front)
{
    return queue[front];
}

int main()
{
    int t;
    cin >> t;
    int queue[t];
    int front = 0, rear = 0;
    int arraySize = t; // Size of the array

    for (int i = 0; i < t; i++)
    {
        char ch;
        cin >> ch;
        if (ch == 'E')
        {
            int e;
            cin >> e;
            enqueue(queue, e, rear, arraySize);
            cout << count << endl;
        }
        else if (ch == 'D')
        {
            int d = dequeue(queue, front, rear);
            cout << d << " " << count << endl;
        }
    }
    return 0;
}

/*
    sample input

    10
    D
    D
    E 51
    E 64
    E 44
    D
    E 54
    E 74
    D
    E 47

    sample output

    -1 0
    -1 0
    1
    2
    3
    51 2
    3
    4
    64 3
    4

*/