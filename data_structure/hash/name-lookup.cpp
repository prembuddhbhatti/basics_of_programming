#include <bits/stdc++.h>
using namespace std;
int main()
{
    map<long long, string> m;
    long long t, q, a, d;
    string s;
    cin >> t;

    while (t--)
    {
        cin >> a;
        cin >> s;
        m[a] = s;
    }

    cin >> q;

    while (q--)
    {
        cin >> d;
        cout << m.at(d) << "\n";
    }
    return 0;
}