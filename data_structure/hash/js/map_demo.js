const myMap = new Map();

myMap.set("Nathan", "555-0182");
myMap.set("Jane", "315-0322");
myMap.set("Jane");
myMap.set("a", ["a"]);
if (myMap.get("a")) {
	let arr = myMap.get("a");
	arr.push("b");
}
for (let [key, value] of myMap) {
	console.log(`${key} = ${value}`);
}

console.log(myMap[0]);
arr = [
	["c", [50]],
	["a", [50]],
	["b", [50]],
];
console.log(arr[0][1][0]);
