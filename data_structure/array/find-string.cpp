#include <iostream>
using namespace std;

#include <vector>
#include <bits/stdc++.h>
#define ll long long

int main()

{
  int t;
  cin >> t;

  while (t--)
  {
    ll row, col;
    cin >> row >> col;
    vector<vector<char>> arr(row, vector<char>(col));
    for (int i = 0; i < row; i++)
    {
      for (int j = 0; j < col; j++)
      {
        cin >> arr[i][j];
      }
    }

    string str;
    cin >> str;

    int i = 0, index = 0;

    while (index < str.size())
    {
      bool flag = false;

      for (int j = 0; j < col; j++)
      {
        if (str[index] == arr[i][j])
        {
          arr[i][j] = '.';
          index++;
          i = (i + 1) % row;
          flag = true;
        }
      }
      if (!flag)
      {
        cout << "No";
        return 0;
      }
    }
    cout << "Yes" << endl;
  }
}