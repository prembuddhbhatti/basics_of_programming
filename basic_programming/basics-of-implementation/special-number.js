process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	let inputArr = stdin_input.split("\n");
	main(inputArr);
});
function special(num) {
	let sum = 0,
		rem = 0,
		n = num;
	while (num) {
		rem = num % 10;
		sum = sum + rem;
		num = Math.floor(num / 10);
	}
	if (sum % 4 == 0) {
		process.stdout.write(n + "\n");
		return 1;
	}
	special(n + 1);
}

function main(input) {
	for (i = 1; i <= input[0]; i++) {
		let n = parseInt(input[i]);
		special(n);
	}
	// Writing output to STDOUT
}

/**
    sample input
    2
    432
    99
 */

/**
        sample output
        435
        103
*/
