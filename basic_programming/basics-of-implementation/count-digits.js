process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	for (let i = 0; i <= 9; i++) {
		const rgx = new RegExp(`[${i}]`, "g");
		const match = input.match(rgx);
		if (match) {
			process.stdout.write(i + " " + match.length + "\n");
		} else {
			process.stdout.write(i + " " + 0 + "\n");
		}
	}
	//process.stdout.write("Hi, " + input + ".\n");       // Writing output to STDOUT
}

//sample input
/**
    77150
 */

//sample output
/**
0 1
1 1
2 0
3 0
4 0
5 1
6 0
7 2
8 0
9 0
 */
