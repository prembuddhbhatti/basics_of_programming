process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	const inpArr = stdin_input.split("\n");
	main(inpArr);
});

function main(input) {
	const d = parseInt(input[0]);
	const oArr = input[1].split(" ");
	const cArr = input[2].split(" ");
	const oc = parseInt(oArr[0]);
	const oF = parseInt(oArr[1]);
	const od = parseInt(oArr[2]);
	const cs = parseInt(cArr[0]);
	const cb = parseInt(cArr[1]);
	const cm = parseInt(cArr[2]);
	const cd = parseInt(cArr[3]);

	let online = oc + (d - oF) * od;
	let offline = cb + (d / cs) * cm + d * cd;
	if (online < offline) {
		process.stdout.write("Online Taxi");
	} else {
		process.stdout.write("Classic Taxi");
	}
	// Writing output to STDOUT
}

/**
    //? sample input
    13
    6 7 4
    4 2 1 2

    //?output
    Online Taxi
 */
