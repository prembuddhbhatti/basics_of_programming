importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

function getBitCt(n) {
	let ct = 0;
	while (n > 0) {
		++ct;
		n >>= 1;
	}
	return ct;
}

var sc = new Scanner(System["in"]); // Reading input from STDIN
var t = sc.nextLine();
for (i = 0; i < t; i++) {
	let n = sc.next();
	let ans = 0;
	//System.out.println("n:"+n);
	for (j = 1; j <= n; j++) {
		if (getBitCt(j) >= getBitCt(Math.floor(n / j))) {
			++ans;
		}
	}
	System.out.println(ans + "");
}
/**
 *  sample input
    
    2
    3
    5

    sample output
    
    2
    4
 */
