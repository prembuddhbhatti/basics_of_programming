importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]);
let t = sc.nextInt();
let rgx = /[1]/g;

while (t--) {
	let l = sc.nextInt();
	let r = sc.nextInt();
	let k = sc.nextInt();
	let f = 0;
	for (i = l; i <= r; i++) {
		if (i.toString(2).match(rgx).length == k) {
			System.out.println(i + "");
			f = 1;
			break;
		}
	}
	if (!f) System.out.println("-1");
}

/**
    sample input
    4
    1 10 1
    1 10 2
    1 10 3
    1 10 4

    output

    1
    3
    7
    -1
 */
