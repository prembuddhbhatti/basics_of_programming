process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	input = input.slice(input.search(/\n/));
	const newStr = input.replaceAll(".", "B");
	const rgx = /HH/;
	const check = newStr.search(rgx);
	//console.log(check);
	if (check !== -1) {
		process.stdout.write("NO");
	} else {
		process.stdout.write("YES" + newStr);
		// Writing output to STDOUT
	}
}

/**
    //?sample input
    5
    H...H

    //?sample input
    YES
    HBBBH
 */
