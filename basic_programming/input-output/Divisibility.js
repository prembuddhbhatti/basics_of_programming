importPackage(java.io);
importPackage(java.lang);
importPackage(java.math);
importPackage(java.util);

var sc = new Scanner(System["in"]); // Reading input from STDIN

var t = sc.nextLine();
let num = "";
for (let i = 0; i < t; i++) {
	var n = parseInt(sc.next()) % 10;
	num += n;
}
let res = num[num.length - 1];
//System.out.println(res);
if (res == 0) {
	System.out.println("Yes");
} else {
	System.out.println("No");
}

/**
    //?sample input
    5
    85 25 65 21 84
    //? output
    No
 */
