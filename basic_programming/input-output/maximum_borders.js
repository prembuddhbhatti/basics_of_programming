process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";
let inputArr = [];
process.stdin.on("data", function (input) {
	stdin_input += input; // Reading input from STDIN
});

process.stdin.on("end", function () {
	main(stdin_input);
});

function main(input) {
	inputArr = input.split("\n");
	//console.log(inputArr);
	//process.stdout.write(inputArr);  // Writing output to STDOUT
	for (let i = 1; i < inputArr.length; i++) {
		//console.log(inputArr.length);
		const rowCollArr = inputArr[i].split(" ");
		const row = rowCollArr[0];
		const coll = rowCollArr[1];
		let maxBr = 0;
		for (let r = 1; r <= row; r++) {
			let inColl = inputArr[i + r];
			let ctBr = 0;
			//process.stdout.write(inColl+"\n");
			for (let c = 0; c < coll; c++) {
				if (inColl[c] === "#") {
					ctBr++;
				}
			}
			if (maxBr <= ctBr) {
				maxBr = ctBr;
			}
		}
		process.stdout.write(maxBr.toString() + "\n");
		i = i + parseInt(row);
		//console.log("i:",i)
	}
}

//sample input
/*
    10
2 15
.....####......
.....#.........
7 9
...###...
...###...
..#......
.####....
..#......
...#####.
.........
18 11
.#########.
########...
.........#.
####.......
.....#####.
.....##....
....#####..
.....####..
..###......
......#....
....#####..
...####....
##.........
#####......
....#####..
....##.....
.#######...
.#.........
1 15
.....######....
5 11
..#####....
.#######...
......#....
....#####..
...#####...
8 13
.....######..
......##.....
########.....
...#.........
.............
#######......
..######.....
####.........
7 5
.....
..##.
###..
..##.
.....
..#..
.#...
14 2
..
#.
..
#.
..
#.
..
..
#.
..
..
..
#.
..
7 15
.###########...
##############.
...####........
...##########..
.......#.......
.....#########.
.#######.......
12 6
#####.
###...
#.....
##....
###...
......
.##...
..##..
...#..
..#...
#####.
####..
*/

//sample output
/**
4
5
9
6
7
8
3
1
14
5
 */
